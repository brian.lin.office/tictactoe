import random
import sys


class TTT:
    def __init__(self):
        self.turn = 'O'
        self.rows = []
        self.r = 0
        self.c = 0
        self.input_value = 0
        self.str_row_column = '列'
        self.input_restart = ''
        self.start()

    def prompt_text(self):
        turn_list = [
            '~現在該 ' + self.turn + ' 下囉~',
            '~換 ' + self.turn + ' 下囉~',
            '~輪到 ' + self.turn + ' 下囉~'
        ]
        rd = random.randint(0, len(turn_list) - 1)
        print('\n' + turn_list[rd])

    def init_board(self):
        self.rows = [['-'] * 3 for i in range(3)]

    def print_board(self):
        for i in range(3):
            for j in range(3):
                print(self.rows[i][j], end="")
            print()

    def check_win(self):
        # row
        for i in range(3):
            if self.rows[i][0] == self.turn \
                    and self.rows[i][1] == self.turn \
                    and self.rows[i][2] == self.turn:
                return True
        # column
        for i in range(3):
            if self.rows[0][i] == self.turn \
                    and self.rows[1][i] == self.turn \
                    and self.rows[2][i] == self.turn:
                return True
        # 左上右下交叉
        for i in range(3):
            if self.rows[0][0] == self.turn \
                    and self.rows[1][1] == self.turn \
                    and self.rows[2][2] == self.turn:
                return True
        # 右上左下交叉
        for i in range(3):
            if self.rows[0][2] == self.turn \
                    and self.rows[1][1] == self.turn \
                    and self.rows[2][0] == self.turn:
                return True
        return False

    def check_end(self):
        for i in range(3):
            for j in range(3):
                if self.rows[i][j] == '-':
                    return False
        return True

    def check_input(self):
        self.input_value = int(self.input_value)
        while self.input_value not in [1, 2, 3]:
            try:
                if self.str_row_column == '列':
                    self.input_value = int(input('超出輸入的範圍了唷! 請重新輸入列(1~3):'))
                else:
                    self.input_value = int(input('超出輸入的範圍了唷! 請重新輸入行(1~3):'))
            except ValueError:
                continue
            except KeyboardInterrupt:
                sys.exit('\n遊戲中止')
        self.input_value -= 1

    def input_row_column(self):
        while True:
            try:
                self.input_value = input('請輸入' + self.str_row_column + '(1~3):')
                self.check_input()
                if self.str_row_column == '列':
                    self.str_row_column = '行'
                else:
                    self.str_row_column = '列'
                return self.input_value
            except ValueError:
                print('輸入有誤，', end='')
                continue
            except KeyboardInterrupt:
                sys.exit('\n遊戲中止')

    def input_all(self):
        self.r = self.input_row_column()
        self.c = self.input_row_column()
        print()

    def is_occupied(self):
        is_o = self.rows[self.r][self.c] != '-'
        if is_o:
            print("\n這裡有人畫過了, 請重新輸入!", end='')
        return is_o

    def place_turn(self):
        self.rows[self.r][self.c] = self.turn

    def reset_game(self, win_end):
        if win_end == 'win':
            print(self.turn + "贏了!!")
        elif 'end':
            print("和局餒")
        self.print_board()
        if self.is_restart():
            self.reset_turn()
            self.init_board()
        else:
            sys.exit('遊戲結束')

    def is_restart(self):
        try:
            self.input_restart = str(input('是否重啟新局?(Y/N)')).upper()
        except KeyboardInterrupt:
            sys.exit('\n遊戲中止')
        while self.input_restart not in ['Y', 'N']:
            try:
                self.input_restart = str(input('輸入錯誤, 是否重啟新局?(Y/N)')).upper()
            except KeyboardInterrupt:
                sys.exit('\n遊戲中止')
        if self.input_restart == 'Y':
            return True
        else:
            return False

    def reset_turn(self):
        self.turn = 'O'

    def change_turn(self):
        if self.turn == 'O':
            self.turn = 'X'
        else:
            self.turn = 'O'

    def start(self):
        self.init_board()
        while True:
            self.prompt_text()
            self.print_board()
            self.input_all()
            if self.is_occupied():
                continue
            else:
                self.place_turn()
                if self.check_win():
                    self.reset_game('win')
                elif self.check_end():
                    self.reset_game('end')
                else:
                    self.change_turn()
