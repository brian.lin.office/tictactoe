import random

turn = 'O'
rows = []
r = 0
c = 0


def prompt_text():
    rd = random.randint(1, 3)
    rd = str(rd)
    print()
    return {
        '1': '~現在該 ' + turn + ' 下囉~',
        '2': '~換 ' + turn + ' 下囉~',
        '3': '~輪到 ' + turn + ' 下囉~'
    }.get(rd, "")


def init_board():
    global rows
    rows = [['-'] * 3 for i in range(3)]


def print_board():
    print(prompt_text())
    for i in range(3):
        for j in range(3):
            print(rows[i][j], end="")
        print()


def check_win():
    # row
    for i in range(3):
        if rows[i][0] == turn \
                and rows[i][1] == turn \
                and rows[i][2] == turn:
            return True
    # column
    for i in range(3):
        if rows[0][i] == turn \
                and rows[1][i] == turn \
                and rows[2][i] == turn:
            return True
    # 左上右下交叉
    for i in range(3):
        if rows[0][0] == turn \
                and rows[1][1] == turn \
                and rows[2][2] == turn:
            return True
    # 右上左下交叉
    for i in range(3):
        if rows[0][2] == turn \
                and rows[1][1] == turn \
                and rows[2][0] == turn:
            return True
    return False


def check_end():
    for i in range(3):
        for j in range(3):
            if rows[i][j] == '-':
                return False
    return True


def reset_game(win_end):
    if win_end == 'w':
        print('\n' + turn + "贏了!! 重啟新局", end='')
    else:
        print("\n和局餒, 重啟新局", end='')
    reset_turn()
    init_board()


def reset_turn():
    global turn
    turn = 'O'


def check_input(input_value, rc):
    while input_value not in [1, 2, 3]:
        if rc == 'r':
            input_value = int(input('超出輸入的範圍了唷! 請重新輸入列(1~3):'))
        else:
            input_value = int(input('超出輸入的範圍了唷! 請重新輸入行(1~3):'))
    input_value -= 1
    return input_value


def input_row():
    return input_row_column('r')


def input_column():
    return input_row_column('c')


def input_row_column(rc):
    rc_text = '列' if rc == 'r' else '行'
    while True:
        try:
            i_rc = input('請輸入' + rc_text + '(1~3):')
            i_rc = check_input(int(i_rc), rc)
            return i_rc
        except ValueError:
            print('輸入有誤，', end='')
            continue
        except KeyboardInterrupt:
            print('\n遊戲中止', end='')
            quit()


def input_all():
    global r, c
    r = input_row()
    c = input_column()


def change_turn():
    global turn
    if turn == 'O':
        turn = 'X'
    else:
        turn = 'O'


def is_occupied():
    is_o = rows[r][c] != '-'
    if is_o:
        print("\n這裡有人畫過了, 請重新輸入!", end='')
    return is_o


def place_turn():
    rows[r][c] = turn


def start():
    init_board()
    while True:
        print_board()
        input_all()
        if is_occupied():
            continue
        else:
            place_turn()
            if check_win():
                reset_game('w')
            elif check_end():
                reset_game('e')
            else:
                change_turn()
