import tkinter as tk
import re

star = ""
which_star_label = ""


def check_date(check_input):
    if len(check_input) == 4 and 1231 >= int(check_input) >= 101:
        return check_input
    else:
        check_input = '0140'
        return check_input


def check_constellation():
    global star, which_star_label
    checked = check_date(date_entry.get())
    date = re.findall(r'.{2}', checked)
    month = int(date[0])
    day = int(date[1])
    constellation = [
        '摩羯座', '水瓶座', '雙魚座', '牡羊座', '金牛座', '雙子座',
        '巨蟹座', '獅子座', '處女座', '天秤座', '天蠍座', '射手座', '摩羯座'
    ]
    limit = [
        20, 18, 20, 20, 21, 21,
        22, 22, 22, 23, 22, 21, 20
    ]
    dat_limit_per_month = [
        31, 29, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31, 31
    ]
    if day <= limit[month - 1]:  # 1/40  if 40 <= 20
        star = constellation[month - 1]
    elif day <= dat_limit_per_month[month - 1]:
        star = constellation[month]
    else:
        star = '輸入的日期無效'
    which_star_label['text'] = star


def btn_reset_click():
    global which_star_label
    print('click 重設 :)')
    date_entry.delete(0, "end")
    which_star_label['text'] = ""


def validate(p):
    if str.isdigit(p) or p == '':
        return True
    else:
        return False


if __name__ == '__main__':
    window = tk.Tk()
    window.title("星座查詢系統")
    window.geometry("300x150")

    # top frame
    date_label = tk.Label(window, text="生日", fg='black')
    date_label.grid(row=0, column=0, sticky=tk.W, pady=2)

    vcmd = (window.register(validate), '%P')
    date_entry = tk.Entry(window, validate='key', validatecommand=vcmd)
    date_entry.grid(row=0, column=1, pady=2)

    confirm_button = tk.Button(window, text="確定", command=check_constellation)
    confirm_button.grid(row=1, column=0, pady=3)
    reset_button = tk.Button(window, text="重設", command=btn_reset_click)
    reset_button.grid(row=1, column=1, pady=3)

    star_label = tk.Label(window, text="星座：", fg='black')
    star_label.grid(row=2, column=0)
    which_star_label = tk.Label(window, text=star, fg='black')
    which_star_label.grid(row=2, column=1)

    window.mainloop()
