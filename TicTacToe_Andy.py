import random

rows = []
turn = 'O'
r = 0
c = 0


def start():
    init_board()
    while True:
        print_board()
        input_row_col()
        if is_occupied():
            continue
        else:
            assign_turn()
            print_board()
            if end_game():
                init_turn()
                init_board()
            else:
                change_turn()


def assign_turn():
    global rows
    rows[r][c] = turn


def is_occupied():
    ret = rows[r][c] != '-'
    if ret:
        print("/n" + "這裡有人畫過了, 請重新輸入!", end='')
    return ret


def input_row_col():
    global r, c
    r = input_row_or_col('row')
    c = input_row_or_col('col')


# 判斷輸贏跟和局
def end_game():
    if check_win():
        print("/n" + turn + "贏了!! 重啟新局", end='')
        return True
    elif check_end():
        print("/n" + "和局餒, 重啟新局", end='')
        return True
    else:
        print('end_game3')
        return False


def prompt_text(tt):
    turn_list = [
        '~現在該 ' + tt + ' 下囉~',
        '~換 ' + tt + ' 下囉~',
        '~輪到 ' + tt + ' 下囉~'
    ]
    rd = random.randint(0, len(turn_list) - 1)
    return turn_list[rd]


def init_board():
    global rows
    rows = [['-'] * 3 for _ in range(3)]


def print_board():
    print(prompt_text(turn))
    for i in range(3):
        for j in range(3):
            print(rows[i][j], end="")
        print()


def check_win():
    # row
    for i in range(3):
        if rows[i][0] == turn \
                and rows[i][1] == turn \
                and rows[i][2] == turn:
            return True
    # column
    for i in range(3):
        if rows[0][i] == turn \
                and rows[1][i] == turn \
                and rows[2][i] == turn:
            return True
    # 左上右下交叉
    for i in range(3):
        if rows[0][0] == turn \
                and rows[1][1] == turn \
                and rows[2][2] == turn:
            return True
    # 右上左下交叉
    for i in range(3):
        if rows[0][2] == turn \
                and rows[1][1] == turn \
                and rows[2][0] == turn:
            return True
    return False


def check_end():
    for i in range(3):
        for j in range(3):
            if rows[i][j] == '-':
                return False
    return True


def check_input(input_value, row_or_col):
    while input_value not in [0, 1, 2]:
        if row_or_col == 'row':
            input_value = int(input('超出輸入的範圍了唷! 請重新輸入列(1~3):'))
        else:
            input_value = int(input('超出輸入的範圍了唷! 請重新輸入行(1~3):'))
    return input_value


def input_row_or_col(row_or_col):
    str_row_or_col = "行" if row_or_col == "row" else "列"
    while True:
        try:
            ret = input('請輸入' + str_row_or_col + '(1~3):')
            ret = check_input(int(ret), row_or_col)
            return ret
        except ValueError:
            print('輸入有誤，', end='')
            continue
        except KeyboardInterrupt:
            print()
            print('遊戲中止', end='')
            quit()


def init_turn():
    global turn
    turn = 'O'


def change_turn():
    print("change_turn")
    global turn
    if turn == 'O':
        turn = 'X'
    else:
        turn = 'O'
