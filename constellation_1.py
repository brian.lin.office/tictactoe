import tkinter as tk

star = ""
which_star_label = ""


def check_constellation():
    global star, which_star_label
    month = int(month_entry.get())
    day = int(day_entry.get())
    constellation = [
        '摩羯座', '水瓶座', '雙魚座', '牡羊座', '金牛座', '雙子座',
        '巨蟹座', '獅子座', '處女座', '天秤座', '天蠍座', '射手座', '摩羯座'
    ]
    limit = [
        20, 18, 20, 20, 21, 21,
        22, 22, 22, 23, 22, 21, 20
    ]
    dat_limit_per_month = [
        31, 29, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31, 31
    ]
    if day <= limit[month - 1]:  # 1/40  if 40 <= 20
        star = constellation[month - 1]
    elif day <= dat_limit_per_month[month - 1]:
        star = constellation[month]
    else:
        star = '輸入的日期無效'
    which_star_label['text'] = star
    # 摩羯座(12 / 22 - 1 / 20): Capricorn 12/22  1/20
    # 水瓶座(1 / 21 - 2 / 18): Aquarius
    # 雙魚座(2 / 19 - 3 / 20): Pisces
    # 牡羊座(3 / 21 - 4 / 20): Aries
    # 金牛座(4 / 21 - 5 / 21): Taurus
    # 雙子座(5 / 22 - 6 / 21): Gemini

    # 巨蟹座(6 / 22 - 7 / 22): Cancer
    # 獅子座(7 / 23 - 8 / 22): Leo
    # 處女座(8 / 23 - 9 / 22): Virgo
    # 天秤座(9 / 23 - 10 / 23): Libra
    # 天蠍座(10 / 24 - 11 / 22): Scorpio
    # 射手座(11 / 23 - 12 / 21): Sagittarius


def btn_reset_click():
    global which_star_label
    print('click 重設 :)')
    month_entry.delete(0, "end")
    day_entry.delete(0, "end")
    which_star_label['text'] = ""


def validate(p):
    if str.isdigit(p) or p == '':
        return True
    else:
        return False


if __name__ == '__main__':
    window = tk.Tk()
    window.title("星座查詢系統")
    window.geometry("300x150")

    # top frame
    month_label = tk.Label(window, text="月份", fg='black')
    month_label.grid(row=0, column=0, sticky=tk.W, pady=2)
    day_label = tk.Label(window, text="日期", fg='black')
    day_label.grid(row=1, column=0, sticky=tk.W, pady=2)

    vcmd = (window.register(validate), '%P')
    month_entry = tk.Entry(window, validate='key', validatecommand=vcmd)
    month_entry.grid(row=0, column=1, pady=2)
    day_entry = tk.Entry(window, validate='key', validatecommand=vcmd)
    day_entry.grid(row=1, column=1, pady=2)

    confirm_button = tk.Button(window, text="確定", command=check_constellation)
    confirm_button.grid(row=0, column=2, pady=3)
    reset_button = tk.Button(window, text="重設", command=btn_reset_click)
    reset_button.grid(row=1, column=2, pady=3)

    star_label = tk.Label(window, text="星座：", fg='black')
    star_label.grid(row=5, column=0)
    which_star_label = tk.Label(window, text=star, fg='black')
    which_star_label.grid(row=5, column=1)

    window.mainloop()
